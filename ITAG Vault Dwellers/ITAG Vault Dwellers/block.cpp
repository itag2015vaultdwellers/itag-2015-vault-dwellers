#include "block.h"
block::block(std::string spritename,char* image_file,int X,int Y):baseSprite(spritename,image_file)
{

	blockhieght=54;
	set_hitbox(X*blockhieght,Y*blockhieght,blockhieght,blockhieght);
	//clearGraphic
	xpos=X;
	ypos=Y;


	isDead=false;
}

void block::setIMAGEPtrs(SDL_Surface* imageptr1,SDL_Surface* imageptr2,SDL_Surface* imageptr3){
	blockIMAGE1=imageptr1;

	sprite_image=blockIMAGE1;
}


bool block::render(SDL_Surface* screen){

	sprite_image=blockIMAGE1;

	baseSprite::render(screen);
	return true;
}
bool block::update(){

	set_hitbox(xpos*blockhieght,ypos*blockhieght,blockhieght,blockhieght);
	
	//check if next to torch

	return true;
}
block::~block()
{
}
void block::setXPOS(int X){
	xpos=X;
}
int block::getXPOS(){
	return xpos;
}
void block::setYPOS(int Y){
	ypos=Y;
}
int block::getYPOS(){
	return ypos;
}	
bool block::getIsDead(){
	return isDead;
}
