#ifndef PLAYER_H
#define PLAYER_H
#include "block.h"
//#include "Base.h"
#include <algorithm>

class player:public block
{
public:
	player(std::string spritename,char* image_file,int X,int Y,int order);
	~player();
	void setIMAGEPtrs(SDL_Surface* imageptr1,SDL_Surface* imageptr2,SDL_Surface* imageptr3);


	bool update();
	bool render(SDL_Surface* screen);
	std::string getPlayerID();
	void setPlayerID(std::string id);
	virtual void setYPOS(int Y);
	virtual void setXPOS(int X);


	int playerNum;
	void Dead();

	int getMineCount();
private:

	std::string playerID;


	SDL_Surface* buildIMAGE;

};
#endif