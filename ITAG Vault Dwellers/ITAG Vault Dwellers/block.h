#ifndef BLOCK_H
#define BLOCK_H

#include "baseSprite.h"

#include <vector>
#include <string>

class block:public baseSprite
{
public:
	enum States{CLEAR=0,PLAYER,BLOCK,TOWER,BASE,MINE,TORCH};
	enum direc{UP,DOWN,LEFT,RIGHT};

	block(std::string spritename,char* image_file,int X,int Y);
	~block();
	virtual void setIMAGEPtrs(SDL_Surface* imageptr1,SDL_Surface* imageptr2,SDL_Surface* imageptr3);


	virtual bool update();
	virtual bool render(SDL_Surface* screen);





	virtual void setXPOS(int X);
	int getXPOS();
	virtual void setYPOS(int Y);
	int getYPOS();




	bool getIsDead();
	
	//ability ressponse





private:


	SDL_Surface* blockIMAGE1;





	
protected:
	bool isDead;

	int xpos;
	int ypos;
	double blockhieght;

};

#endif