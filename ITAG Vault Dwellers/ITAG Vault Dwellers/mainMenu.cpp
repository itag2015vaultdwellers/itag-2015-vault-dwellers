#include "mainMenu.h"
namespace mM{
	mainMenu::mainMenu()
	{
		//The font that's going to be used
		font = NULL;
		//The color of the font
		SDL_Color theColor = { 0, 0, 255 };
		textColor = theColor;

		//set surfaces to null
		screen = NULL;
		background = NULL;
		title=NULL;
		playGame1=NULL;
		exit-NULL;

		running=true;
		runGame1=false;
	    //set music to null
		b_music=NULL;
		swarm=NULL;

		upPressed=false;
		downPressed=false;
		moveDown=false;
		moveUp=false;
		option=0;
		returnPressed=false;
		bool doOption=false;
	}
	bool mainMenu::oninit()
	{
		if (SDL_Init( SDL_INIT_VIDEO )<0){
			return false;
		}//can use init_everything, but this is sufficient

		//basic size and other data
		if ((screen = SDL_SetVideoMode( 640, 480, 32, SDL_SWSURFACE ))==NULL){
			return false;
		}

		//read the file into a surface
		if ((background = SDL_LoadBMP( "newbackground.bmp" ))==NULL){
			return false;
		} 
			//read the file into a surface in a sprite class
		baseSprite Aselector("selector","crate_v1.bmp");
		selector=Aselector;
		if (selector.get_spriteName()=="error"){
			return false;
		}else{
			selector.set_hitbox(150,200,10,10);
		}

		//Initialize SDL_ttf
		if( TTF_Init() == -1 ){
			return false;    
		}
		//If there was an error in loading the font
		font = TTF_OpenFont( "Vera-Bold.ttf", 28 );

		if(font == NULL )
		{
			return false;
		}

		//Set the window caption
		SDL_WM_SetCaption( "TTF Test", NULL );

		//preset messages
		title = TTF_RenderText_Blended(font, "Press enter to play", textColor );
		playGame1 = TTF_RenderText_Blended(font, "game1 ", textColor );
		exit = TTF_RenderText_Blended(font, "exit ", textColor );
		//Initialize SDL_mixer
		if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
		{
			return false;    
		}

		//Load the music
		b_music = Mix_LoadMUS( "b_music.wav" );//background music
		//If there was a problem loading the music
		if( b_music == NULL ){
			return false;    
		}
		//Load the sound effects
		swarm = Mix_LoadWAV( "locustswarmloop.wav" );
		//If there was a problem loading the sound effects
		if(  swarm == NULL ){
			return false;    
		}




		return true;
	}
	int mainMenu::onexecute()
	{
		if (oninit()==false){
			return -1;
		}

		SDL_Event Event;
		while (running){
			while(SDL_PollEvent(&Event)){
				onevent(&Event);
			}
			onloop();
			if(running==true){
				onrender();
			}
		}
		//oncleanup();
		return 0;
	}
	void mainMenu::onevent(SDL_Event* Event)
	{
		// keyboard input is an event
		CEvent::OnEvent(Event);
	}
	void mainMenu::OnExit(){
		running=false;
	}
	void mainMenu::OnKeyDown(SDLKey sym, SDLMod mod, Uint16 unicode)
	{
		switch(sym){
		case SDLK_UP:
			//move up true
			if(upPressed==false){ //only when key changes from up to down does anything happen
			    upPressed=true;
				moveUp=true;
			}

			break;
		case SDLK_DOWN:
			//move up true
			if(downPressed==false){
			    downPressed=true;
				moveDown=true;
			}
			break;
		case SDLK_RETURN:
			//accept choice 
			  //for now just toggle Game1
			if(downPressed==false){
			    downPressed=true;
				doOption=true;
			}
			runGame1==true;

			break;
		default:
			break;
		}
	}
	void mainMenu::OnKeyUp(SDLKey sym, SDLMod mod, Uint16 unicode)
	{
		switch(sym){
		case SDLK_UP:
			//move down true
			if(upPressed==true){
			    upPressed=false;
			}
			break;
		case SDLK_DOWN:
			//move up true
		    if(downPressed==true){
			    downPressed=false;
			}

			break;
		case SDLK_RETURN:
			//move up true
		    if(returnPressed==true){
			    returnPressed=false;
			}
			break;
		default:
			break;
		}
	}
	void mainMenu::oncleanup()
	{
		SDL_FreeSurface(background);
		selector.cleanup_sprite();
		SDL_FreeSurface(screen);


		//Free the sound effects
		Mix_FreeChunk( swarm);
		//Free the music
		Mix_FreeMusic( b_music );

		//free text surface
		SDL_FreeSurface( title );
		SDL_FreeSurface( playGame1 );
		SDL_FreeSurface( exit );

		//Quit SDL_mixer
		Mix_CloseAudio();

		//Close the font that was used
		TTF_CloseFont( font );

		//Quit SDL_ttf
		TTF_Quit();

		SDL_Quit();
	}
	void mainMenu::onloop()
	{
		//Ifmake sure music is playing
		if( Mix_PlayingMusic() == 0 ){
			//Play the music
			if( Mix_PlayMusic( b_music, -1 ) == -1 ){
				//error
				int error=0;
			}    
		}
		if(doOption==true){
			switch(option){
			case GAME1:
				{
					game1::thegame aGame;
					runGame1=aGame.onexecute();
					running=false;
					break;
				}
			case EXIT:
				{
					running=false;
					break;
				}
			default:
				{//some stuf
				}
			}
		}
		//update y pos of selector
		if(moveUp==true){
			if(option>0){
			selector.update_hitbox(0,-50);
			option--;
			}
			moveUp=false;
		}
		if(moveDown==true){
			if(option<1){
			selector.update_hitbox(0,50);
			option++;
			}
			moveDown=false;
		}

	}
	void mainMenu::onrender()
	{
		CSurface::OnDraw(screen,background,0,0);
		selector.draw_sprite(screen);


		CSurface::OnDraw(screen,title, 150, 100);

		CSurface::OnDraw(screen,playGame1, 250, 200);
		CSurface::OnDraw(screen,exit, 250, 250);

		SDL_Flip(screen);
	}
}
