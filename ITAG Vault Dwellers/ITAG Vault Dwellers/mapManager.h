#ifndef MAPMANAGER_H
#define MAPMANAGER_H

#include <map>
#include <vector>
#include <string>
#include <stdlib.h>     
#include <time.h>  


#include "block.h"
#include "player.h"
//#include "Base.h"
//#include "tower.h"

class mapManager
{
public:
	//enum direc{UP,DOWN,LEFT,RIGHT};

	mapManager(int mapX,int mapY);
	~mapManager();

	bool update();
	bool render(SDL_Surface* screen);

	void addplayer(player* aplayer,int x,int Y);
	bool hasPlayer(std::string id);





	bool checkPlayerDead(std::string id);

private:
	std::vector<block*> gameMap;
	std::map<std::string,player*>players;
	block* mainBase;
	//board dimensions
	int maxX;
	int maxY;

	SDL_Surface* blockIMAGE1;




};
#endif