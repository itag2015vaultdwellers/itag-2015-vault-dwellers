#include "mapManager.h"

mapManager::mapManager(int mapX,int mapY)
{
	//load map tile images
	if((blockIMAGE1 = SDL_LoadBMP( "mapTile_v1.bmp" ))==NULL){
		return;
	}else{
		blockIMAGE1=SDL_DisplayFormat( blockIMAGE1);
		//remove white 
		//map colour key
		Uint32 colourkey=SDL_MapRGB(blockIMAGE1->format,0xFF,0xFF,0xFF);
		//set pixels to transparent
		SDL_SetColorKey(blockIMAGE1,SDL_SRCCOLORKEY,colourkey);
	}
	maxX=mapX;
	maxY=mapY;





	//create map




}

mapManager::~mapManager()
{
}

bool mapManager::render(SDL_Surface* screen){
	for(auto m:gameMap){
		m->render(screen);
	}
	return true;
}
bool mapManager::update(){
	for(auto m:gameMap){
		m->update();
	}
	return true;
}
void mapManager::addplayer(player* aplayer,int X,int Y){
	if(aplayer->playerNum==1){
		//aplayer->setIMAGEPtrs(buildIMAGE1,wreckIMAGE1,torcherIMAGE);
	}else if(aplayer->playerNum==2){
		//aplayer->setIMAGEPtrs(buildIMAGE2,wreckIMAGE2,torcherIMAGE);
	}
	int position=(maxY*Y)+X; // set starting position
	//synch self 

	//add to map
	//gameMap.at(position)=aplayer;



	//insert random name generator
	aplayer->setPlayerID("player"+std::to_string(aplayer->playerNum));

	players[aplayer->getPlayerID()]=aplayer;
}
bool mapManager::hasPlayer(std::string id){
	for(auto p:players){
		if(p.first==id){
			return true;
		}
	}
	return false;
}
bool mapManager::checkPlayerDead(std::string id){
	if(hasPlayer(id)==true){
		return players[id]->getIsDead();
	}
	return false;
}



